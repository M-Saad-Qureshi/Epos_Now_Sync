﻿namespace EposAPIHelper
{
    public class ProductCSV
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public int CurrentStock { get; set; }
        public string Barcode { get; set; }
        public string Sku { get; set; }
        public string Mode { get; set; }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EposAPIHelper
{
    class EmailClass
    {
        private string _emailProviderType = "Gmail";

        public string Recipient
        { get; set; }

        public string Sender
        { get; set; }

        public string SenderEmailPassword
        { get; set; }

        public string Body
        { get; set; }

        public string Subject
        { get; set; }

        public string EmailProviderType
        {
            get { return _emailProviderType; }
            set { _emailProviderType = value; }
        }

        public void sendEmail()
        {
            //try
            //{
            EmailSender es = new EmailSender();
            es.From = Sender;
            es.To = Recipient;
            es.Password = SenderEmailPassword;
            es.Subject = Subject;
            es.Body = Body;
            es.IsHtmlBody = false;

            switch (EmailProviderType.ToLower())
            {
                case "yahoo":
                    es.EmailProvider = EmailSender.EmailProviderType.Yahoo;
                    break;
                case "hotmail":
                    es.EmailProvider = EmailSender.EmailProviderType.Hotmail;
                    break;
                case "gmail":
                    es.EmailProvider = EmailSender.EmailProviderType.Gmail;
                    break;
                default:
                    break;
            }
            es.Send();
        }     
    }
}

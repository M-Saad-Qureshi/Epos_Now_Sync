﻿// Generated by Xamasoft JSON Class Generator
// http://www.xamasoft.com/json-class-generator

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EposAPIHelper
{

    public class StaffResponse
    {

        [JsonProperty("StaffID")]
        public int StaffID { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("SwipeLogin")]
        public string SwipeLogin { get; set; }

        [JsonProperty("AccessRightID")]
        public int AccessRightID { get; set; }

        [JsonProperty("HourlyRate")]
        public double HourlyRate { get; set; }
    }

}

﻿// Generated by Xamasoft JSON Class Generator
// http://www.xamasoft.com/json-class-generator

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EposAPIHelper
{

    public class LocationResponse
    {

        [JsonProperty("LocationID")]
        public int LocationID { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Address1")]
        public object Address1 { get; set; }

        [JsonProperty("Address2")]
        public object Address2 { get; set; }

        [JsonProperty("Town")]
        public object Town { get; set; }

        [JsonProperty("County")]
        public object County { get; set; }

        [JsonProperty("PostCode")]
        public object PostCode { get; set; }

        [JsonProperty("EmailAddress")]
        public object EmailAddress { get; set; }
    }

}

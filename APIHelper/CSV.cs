﻿using CsvHelper;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EposAPIHelper
{
    public class CSV
    {
        public CSV(Form form)
        {

        }
        public List<ProductResponse> getListFromCSV(TextReader stream)
        {
            CsvReader csv = new CsvReader(stream);
            return (List<ProductResponse>)csv.GetRecords<ProductResponse>();
        }

        public List<ProductCSV> getProductsFromCSV(string filePath)
        {
            TextReader textReader = new StreamReader(filePath);
            CsvReader csv = new CsvReader(textReader);
            var records = csv.GetRecords<ProductCSV>().ToList();
           
            return (List<ProductCSV>)records;
        }

        public List<ProductDGV> getProductsDGVFromCSV(string filePath)
        {
            try
            {
                TextReader textReader = new StreamReader(filePath);
                CsvReader csv = new CsvReader(textReader);
                var records = csv.GetRecords<ProductDGV>().ToList();

                return (List<ProductDGV>)records;
            }          

             catch (Exception ex)
            {
                var dontKnow = ex.Data["CsvHelper"];
            }

            return new List<ProductDGV>();
        }

        public void exportProductCSVList(List<ProductCSV> records,string filePath)
        {
            TextWriter textWriter = new StreamWriter(@filePath);
            CsvWriter csv = new CsvWriter(textWriter);
            csv.WriteRecords(records);
            textWriter.Close();
        }

        public void exportProductDGVList(List<ProductDGV> records, string filePath)
        {
            TextWriter textWriter = new StreamWriter(@filePath);
            CsvWriter csv = new CsvWriter(textWriter);
            csv.WriteRecords(records);
            textWriter.Close();
        }

        public void exportProductResponseList(List<ProductResponse> records, string filePath)
        {
            TextWriter textWriter = new StreamWriter(@filePath);
            CsvWriter csv = new CsvWriter(textWriter);
            csv.WriteRecords(records);
            textWriter.Close();
        }
    }
}

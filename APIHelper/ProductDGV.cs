﻿using System.ComponentModel;

namespace EposAPIHelper
{
    public class ProductDGV
    {
        private string name;
        private int productId = 0;
        private int currentStock;
        private int changeStock;
        private int locationID;
        private long barcode;

        private string sku;
        private string mode;

        private int stockID;

        public int ProductID
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        /// <summary>
        /// This field represents stock which is on DB
        /// </summary>
        public int CurrentStock
        {
            get
            {
                return currentStock;
            }

            set
            {
                currentStock = value;
            }
        }

        /// <summary>
        /// This field represents stock that will be added, subtracted or replaced
        /// </summary>
        public int ChangeStock
        {
            get
            {
                return changeStock;
            }

            set
            {
                changeStock = value;
            }
        }
        public long Barcode
        {
            get
            {
                return barcode;
            }

            set
            {
                barcode = value;
            }
        }

        public int LocationID
        {
            get
            {
                return locationID;
            }

            set
            {
                locationID = value;
            }
        }

     

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }
        [Browsable(false)]
        public string Mode
        {
            get
            {
                return mode;
            }

            set
            {
                mode = value;
            }
        }

        public int StockID
        {
            get
            {
                return stockID;
            }

            set
            {
                stockID = value;
            }
        }

        public ProductDGV()
        { }
        public ProductDGV(ProductResponse prodcut)
        {
            this.ProductID = prodcut.ProductID;
            this.Name = prodcut.Name;
            if (string.IsNullOrEmpty(prodcut.Barcode) == false)
            {
                this.Barcode = long.Parse(prodcut.Barcode);
            }

            if (prodcut.Sku != null)
            {
                this.Sku = prodcut.Sku.ToString();
            }            
        }

        public ProductDGV(ProductResponse prodcut, ProductStockResponse stock, int locationId)
        {
            this.ProductID = prodcut.ProductID;
            this.Name = prodcut.Name;

            if (string.IsNullOrEmpty(prodcut.Barcode) == false)
            {
                this.Barcode = long.Parse(prodcut.Barcode);
            }           

            if (prodcut.Sku != null)
            {
                this.Sku = prodcut.Sku.ToString();
            }
            this.CurrentStock = stock.CurrentStock;
            this.StockID = stock.StockID;
            this.LocationID = locationId;
        }    
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EposAPIHelper
{
    public static class GlobalClass
    {
        private static Boolean _isUserLogged = false;

        public static Boolean IsUserLoggedIn
        {
            get { return _isUserLogged; }
            set { _isUserLogged = value; }
        }

        public static string Location
        { get; set; }

        public static int LocationId
        { get; set; }

        public static int CurrentUserId
        { get; set; }

        public static string CurrentUserName
        { get; set; }

        public static string EmailRecipeint
        { get; set; }
        public static string EmailSender
        { get; set; }
        public static string EmailPassword
        { get; set; }
        public static string EmailProvider
        { get; set; }
  
    }
}

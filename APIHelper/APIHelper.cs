﻿using HTTPCommunicator;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace EposAPIHelper
{
    public class APIHelper : HTTPHelper
    {

        const string Add = "Add";
        const string Subtract = "Subtract";
        const string Replace = "Replace";


        private static CookieContainer cookieJar = new CookieContainer();

        /**
        Variables for EPOS-Now
        **/

        private static string baseURL = "https://api.eposnowhq.com/api/V1";
        private static string productUrl = "/Product/";
        private static string productStockUrl = "/ProductStock/";
        private static string staffUrl = "/Staff/";
        private static string locationUrl = "/Location/";

        private static string accessToken = "TlBLMUwwMjhYNkQzOFNIVUIzOUpDRk0xSFFMNjdZSDc6Q0o3Vk9BMVJENEowWTdPMEU3WjRZM0IzRjRPS0FRQzg=";
        // You may define a method that can be specific to this Epos-now API

        private static HttpWebRequest getAuthRequest(Form form, string url)
        {
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(url);
            webReq.Headers.Add("Authorization", "Basic " + accessToken);
            return webReq;
        }

        public static StaffResponse authenticateUser(Form form, string user, string password)
        {
            HttpWebRequest webReq = getAuthRequest(form, baseURL + staffUrl);
            string response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);

            List<StaffResponse> staffList = JsonHelper.desrializeStaff(response);

            StaffResponse logginStaff = staffList.Where(s => s.Name.Equals(user) && s.Password.Equals(password)).FirstOrDefault();

            return logginStaff;
        }

        private static string getProducts(Form form)
        {
            HttpWebRequest webReq = getAuthRequest(form, baseURL + productUrl);
            string response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);
            return response;
        }

        private static string getProducts(Form form, String url)
        {
            HttpWebRequest webReq = getAuthRequest(form, url);
            string response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);
            return response;
        }

        /// <summary>
        /// It returns all stock from DB, no paging is required yet
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        private static string getProductStock(Form form)
        {
            HttpWebRequest webReq = getAuthRequest(form, baseURL + productStockUrl);
            string response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);
            return response;
        }

        private static string getAllLocation(Form form)
        {
            HttpWebRequest webReq = getAuthRequest(form, baseURL + locationUrl);
            string response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "GET", "", true);
            return response;
        }

        private static string fetchProducts(Form form)
        {
            string response = getProducts(form);
            return response;
        }

        private static List<ProductDGV> failedList = new List<ProductDGV>();
        private static Dictionary<string, ProductCSV> successList = new Dictionary<string, ProductCSV>();



        /// <summary>
        ///  Epos-Now-Sync
        /// </summary>
        /// <param name="form"></param>
        /// <param name="productList">List which need to be upload on Epos-now</param>
        /// <param name="csvProducts">List of products fetced from CSV (if it is empty/null then it will handeled too)
        ///  which will be added in failedList if they are not in "productList"</param>
        /// <param name="mode"></param>
        public static void changeProducts(Form form, List<ProductDGV> productList,
            List<ProductDGV> csvProducts, string mode)
        {
            List<ProductDGV> updatedProducts = new List<ProductDGV>();

            Label label1 = (Label)form.Controls["labelab"];
            Label label2 = (Label)form.Controls["labelyz"];

            //if (label1 == null || label2 == null)
            //{
            //    return;
            //}

            productList = doModeChanges(productList, mode);

            for (int i = 0; i < productList.Count; i++)
            {
                // productList = doModeChanges()
                if (updateProductStock(form, productList[i]) == true)
                {
                    updatedProducts.Add(productList[i]);
                }

                else
                {   // adding products which were failed to be updated.
                    //Like they may have CurrentStock in -ve whic iss not allowed b API
                    failedList.Add(productList[i]);
                }
            }

            //failedList.AddRange()

            if (csvProducts != null && csvProducts.Count > 0)
            {
                //Removing all which are updated
                csvProducts.RemoveAll(p => updatedProducts.Any(uP => uP.Barcode.Equals(p.Barcode)));

                failedList = csvProducts;

                //var result = csvProducts.Where(p => !peopleList1.Any(p2 => p2.ID == p.ID));
            }

            DataGridView dgvSuccess = form.Controls.Find("dgvUpdatingProduct", true).FirstOrDefault() as DataGridView;
            DataGridView dgvFail = form.Controls.Find("dgvFailedProduct", true).FirstOrDefault() as DataGridView;

            bindListToDGV(dgvSuccess, updatedProducts);
            bindListToDGV(dgvFail, failedList);
        }

        private static ProductStockResponse doModeChanges(ProductResponse product, ProductCSV csvProduct, List<ProductStockResponse> productStockList)
        {
            ProductStockResponse productStock = null;

            for (int i2 = 0; i2 < productStockList.Count; i2++)
            {
                if (product.ProductID.Equals(productStockList[i2].ProductID) == true)
                {
                    productStock = productStockList[i2];
                    break;
                }
            }

            string givenMode = csvProduct.Mode;

            switch (givenMode)
            {
                case Add:
                    {
                        productStock.CurrentStock += csvProduct.CurrentStock;
                        break;
                    }
                case Subtract:
                    {
                        productStock.CurrentStock -= csvProduct.CurrentStock;
                        break;
                    }
                case Replace:
                    {
                        productStock.CurrentStock = csvProduct.CurrentStock;
                        break;
                    }

                default:
                    {
                    }
                    break;
            }

            return productStock;
        }

        /// <summary>
        /// It returns List<ProductDGV> with changes in CurrentStock according to ChangeStock and Mode 
        /// </summary>
        /// <param name="products"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private static List<ProductDGV> doModeChanges(List<ProductDGV> products, string mode)
        {
            string givenMode = mode;

            foreach (ProductDGV product in products)
            {
                switch (givenMode)
                {
                    case Add:
                        {
                            product.CurrentStock += product.ChangeStock;
                            break;
                        }
                    case Subtract:
                        {
                            product.CurrentStock -= product.ChangeStock;
                            break;
                        }
                    case Replace:
                        {
                            product.CurrentStock = product.ChangeStock;
                            break;
                        }

                    default:
                        {
                        }
                        break;
                }
            }

            return products;
        }

        private static Dictionary<string, ProductCSV> fillFailed(List<ProductCSV> productCSVList,
             List<ProductResponse> productList)
        {

            List<ProductResponse> productDisticntList = productList.Where(item => item.Barcode != null).Distinct().ToList();

            HashSet<string> productBarcodes = new HashSet<string>(productDisticntList.Select(s => s.Barcode));

            Dictionary<string, ProductCSV> results = productCSVList.Where(productCSV =>
           !productBarcodes.Contains(productCSV.Barcode)).ToDictionary(s => s.Barcode, s => s);

            return results;
        }

        private static bool updateProductStock(Form form, ProductStockResponse productStock)
        {
            bool successful = true;

            HttpWebRequest webReq = getAuthRequest(form, baseURL + productStockUrl + productStock.StockID);

            var p = new { CurrentStock = productStock.CurrentStock };
            //string json = JsonConvert.SerializeObject(p, Formatting.Indented);

            string data = JsonHelper.serializeObject(p, false);
            //string data = "blah blah";

            string response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "PUT", data, true);

            if (response.Contains(productStock.StockID.ToString()) == false)
            {
                successful = false;
            }

            return successful;
        }

        private static bool updateProductStock(Form form, ProductDGV product)
        {
            bool successful = true;

            HttpWebRequest webReq = getAuthRequest(form, baseURL + productStockUrl + product.StockID);

            var p = new { CurrentStock = product.CurrentStock };
            //string json = JsonConvert.SerializeObject(p, Formatting.Indented);

            string data = JsonHelper.serializeObject(p, false);
            //string data = "blah blah";

            string response = HTTPHelper.GetResponse(webReq, cookieJar, "", false, "PUT", data, true);

            if (response.Contains(product.StockID.ToString()) == false)
            {
                successful = false;
            }

            return successful;
        }


        /// <summary>
        /// It returns ProductCSV object equivalent to ProductResponse and ProductStock
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="productList"></param>
        /// <param name="productCSVList"></param>
        /// <returns></returns>
        private static ProductCSV getProdcutCSV(int productId, List<ProductResponse> productList, List<ProductCSV> productCSVList)
        {
            for (int i = 0; i < productCSVList.Count; i++)
            {
                for (int i2 = 0; i2 < productList.Count; i2++)
                {
                    if (productId.Equals(productList[i2].ProductID) && productCSVList[i].Barcode.Equals(productList[i2].Barcode))
                    {
                        return productCSVList[i];
                    }
                }
            }

            return null;
        }

        private static void bindDictionaryToDGV(DataGridView dgv, Dictionary<string, ProductCSV> productCSVDictionary)
        {
            dgv.DataSource = null;
            dgv.Columns.Clear();
            dgv.Refresh();

            List<ProductCSV> prodcutCSVList = new List<ProductCSV>();

            foreach (KeyValuePair<string, ProductCSV> entry in productCSVDictionary)
            {
                prodcutCSVList.Add(entry.Value);
            }
            dgv.DataSource = prodcutCSVList;
            dgv.AutoResizeColumns();
        }
        private static void bindListToDGV(DataGridView dgv, List<ProductDGV> productList)
        {
            dgv.DataSource = null;
            dgv.Columns.Clear();
            dgv.Refresh();

            dgv.DataSource = productList;
            dgv.AutoResizeColumns();
        }

        public static void autoBind(DataGridView dgv, List<ProductCSV> productCSVList)
        {
            dgv.DataSource = productCSVList;
        }

        public static bool isLocationAuthenticated(Form form, string location, int locationId)
        {
            bool authenticated = false;

            string response = getAllLocation(form);

            List<LocationResponse> locationList = JsonHelper.desrializeLocations(response);

            authenticated = locationList.Any(s => s.Name.Equals(location) && s.LocationID.Equals(locationId));

            return authenticated;
        }

        public static List<ProductDGV> getProductDGVF(Form form)
        {
            String response = APIHelper.getProducts(form);
            List<ProductResponse> productList = EposAPIHelper.JsonHelper.deserializProducts(response);

            List<ProductDGV> productDGVList = new List<ProductDGV>();

            for (int i = 0; i < productList.Count; i++)
            {
                productDGVList.Add(new ProductDGV(productList[i]));
            }

            return productDGVList;

        }

        public static List<ProductDGV> getProductDGV(Form form, int locationId)
        {
            CSV csv = new CSV(form);

            //All products
            List<ProductResponse> productList = getAllProducts(form);
            //csv.exportProductResponseList(productList,"products");

            //All stock
            List<ProductStockResponse> stockList = getAllProductStock(form);

            List<ProductDGV> productDGVList = fillProductDGV(form, locationId, productList, stockList);
            //csv.exportProductDGVList(productDGVList, "productsDGV");

            return productDGVList;
        }

        /// <summary>
        /// It will return "List<ProductResponse>" with all products from DB
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        private static List<ProductResponse> getAllProducts(Form form)
        {
            List<ProductResponse> productList = new List<ProductResponse>();

            int pageNo = 1;
            String url = "";
            String response = "";

            for (int i = 0; i < 1000000; i++)
            {
                url = baseURL + productUrl.TrimEnd('/') + "?page=" + pageNo++;
                response = APIHelper.getProducts(form, url);

                // its mean paging has reach at its end
                if (response.Length == 2 && response.Equals("[]"))
                {
                    break;
                }

                productList.AddRange(EposAPIHelper.JsonHelper.deserializProducts(response));
            }
            return productList;
        }

        /// <summary>
        /// It will return "List<ProductStockResponse>" with all Stock from DB
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        private static List<ProductStockResponse> getAllProductStock(Form form)
        {
            List<ProductStockResponse> stockList = new List<ProductStockResponse>();
            String response = APIHelper.getProductStock(form);
            stockList.AddRange(EposAPIHelper.JsonHelper.deserializProductStock(response));
            return stockList;
        }


        /// <summary>
        /// It returns a "List<ProductDGV>" which is represents product of specific location and stock
        /// </summary>
        /// <param name="form"></param>
        /// <param name="locationId"></param>
        /// <param name="productList"></param>
        /// <param name="stockList"></param>
        /// <returns></returns>
        private static List<ProductDGV> fillProductDGV(Form form, int locationId,
            List<ProductResponse> productList, List<ProductStockResponse> stockList)
        {
            List<ProductDGV> productDGVList = new List<ProductDGV>();

            for (int i = 0; i < productList.Count; i++)
            {
                for (int i2 = 0; i2 < stockList.Count; i2++)
                {
                    if (productList[i].ProductID.Equals(stockList[i2].ProductID) &&
                        stockList[i2].LocationID.Equals(locationId))
                    {
                        productDGVList.Add(new ProductDGV(productList[i], stockList[i2], locationId));
                    }
                }
            }
            return productDGVList;
        }

        /// <summary>
        /// It return list of products that has been updated by the quantity of n
        /// </summary>
        /// <param name="form"></param>
        /// <param name="productList"></param>
        /// <param name="barcode"></param>
        /// <param name="qunatity"></param>
        /// <returns></returns>
        public static List<ProductDGV> updateProduct(Form form, List<ProductDGV> productList, long barcode, int qunatity)
        {
            List<ProductDGV> updateList = new List<ProductDGV>();

            foreach (ProductDGV product in productList)
            {
                if (product.Barcode.Equals(barcode))
                {
                    product.ChangeStock += qunatity;
                    updateList.Add(product);
                    // I should add break here if list has single products
                }
            }

            return updateList;
        }

        /// <summary>
        /// It return list of products that have been updated by the quantity defined in CSV.
        ///  csvList is matched with productList by Barcode and is added in the return list with defined quantity.
        /// </summary>
        /// <param name="form"></param>
        /// <param name="productList"></param>
        /// <param name="csvList"></param>    
        /// <returns></returns>
        public static List<ProductDGV> updateProduct(Form form, List<ProductDGV> productList,
            List<ProductDGV> csvList)
        {
            List<ProductDGV> updateList = new List<ProductDGV>();

            foreach (ProductDGV product in productList)
            {
                foreach (ProductDGV csv in csvList)
                {
                    if (product.Barcode.Equals(csv.Barcode))
                    {
                        product.ChangeStock = csv.ChangeStock;
                        updateList.Add(product);
                        // I should add break here if list has single products
                    }
                }
            }

            return updateList;
        }

        /// <summary>
        /// Writes email from sender id and send to recipient id.
        /// </summary>
        /// <param name="body"></param>
        /// <param name="Recipient"></param>
        /// <param name="Sender"></param>
        /// <param name="Password"></param>
        /// <param name="emailProvider">It can be only Yahoo,Gmail,Hotmail</param>
        /// <param name="fileName">Path to file to attach</param>
        public static void mailTo(string body, string Recipient, string Sender, string Password, string emailProvider, string fileName)
        {
            EmailClass emailer = new EmailClass();

            emailer.Recipient = Recipient;
            emailer.Sender = Sender;

            emailer.SenderEmailPassword = Password;
            emailer.Subject = "Epos-Now Product Update Report of: " + DateTime.Now.ToString();
            emailer.Body = body;
            emailer.EmailProviderType = emailProvider;

            emailer.sendEmail();
        }
    }
}












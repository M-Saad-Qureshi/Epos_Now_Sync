﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EposAPIHelper
{
    public class JsonHelper
    {
        public static List<ProductResponse> deserializProducts(String response)
        {
            return JsonConvert.DeserializeObject<List<ProductResponse>>(response);
        }

        public static List<ProductStockResponse> deserializProductStock(String response)
        {
            return JsonConvert.DeserializeObject<List<ProductStockResponse>>(response);
        }

        public static String serializeObject(dynamic anyObject, bool excludeNulls)
        {
            string json = "";
            if (excludeNulls == true)
            {
                json = JsonConvert.SerializeObject(anyObject, Newtonsoft.Json.Formatting.None,
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            }
            else
            {
                json = JsonConvert.SerializeObject(anyObject);
            }

            return json;

        }

        public static List<StaffResponse> desrializeStaff(string response)
        {
            return JsonConvert.DeserializeObject<List<StaffResponse>>(response);
        }

        public static List<LocationResponse> desrializeLocations(string response)
        {
            return JsonConvert.DeserializeObject<List<LocationResponse>>(response);
        }
    }
}

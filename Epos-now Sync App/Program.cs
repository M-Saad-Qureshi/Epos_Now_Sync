﻿using System;
using System.Windows.Forms;

namespace Epos_now_Sync_App
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            LoginForm fLogin = new LoginForm();

        askLogin:
            if (!fLogin.IsDisposed) 
            {
                if (fLogin.ShowDialog() == DialogResult.OK)
                {
                    Application.Run(new MainForm());
                }

                else
                {
                    goto askLogin;
                }
            }

            else
            {
                Application.Exit();
            }

            //do
            //{

            //} while (fLogin.ShowDialog() != DialogResult.OK);

            //Application.Run(new MainForm());



        }
    }
}

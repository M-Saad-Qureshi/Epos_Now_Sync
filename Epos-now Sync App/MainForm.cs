﻿using EposAPIHelper;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace Epos_now_Sync_App
{
    public partial class MainForm : Form
    {
        ///-- Global variable--\\\

        /// <summary>
        /// List for total products
        /// </summary>
        List<ProductDGV> onlineProducts;

        // List for successful products
        List<ProductDGV> updatedProducts = new List<ProductDGV>();

        /// <summary>
        ///  List of products loaded from CSV
        /// </summary>
        List<ProductDGV> productsCsv;

        ///-- Global refernce for controls--\\\
        RadioButton rdioBtnSelectedMode;

        public MainForm()
        {
            InitializeComponent();
            toolTip1.SetToolTip(this.btnLoadCSV, "You can upload CSV (containing updating info) instead of using barcode" +
                "to update products");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            KeyValuePair<string, int> location = getCurrentLocation();

            if (APIHelper.isLocationAuthenticated(this, location.Key, location.Value))
            {
                label1.Text = location.Key;

                GlobalClass.Location = location.Key;
                GlobalClass.LocationId = location.Value;
            }

            // Need to close app if location is not correct         
        }


        private KeyValuePair<string, int> getCurrentLocation()
        {
            KeyValuePair<string, int> location;

            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            string key = appSettings.GetKey(0);
            int value = int.Parse(appSettings[0]);
            location = new KeyValuePair<string, int>(key, value);
            return location;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control control in this.grpBxModes.Controls)
            {
                if (control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;
                    if (radio.Checked)
                    {
                        rdioBtnSelectedMode = radio;
                    }
                }
            }
        }

        private void btnLoadProducts_Click(object sender, EventArgs e)
        {
            // it means selection still is not made yet.
            if (grpBxModes.Enabled)
            {
                MessageBox.Show("Select mode first, then confirm your selection. After that you can load products",
                    "Infomation", MessageBoxButtons.OK);
            }
            else
            {
                TSLblStatus.Text = "Loading products, please wait..";
                onlineProducts = APIHelper.getProductDGV(this, GlobalClass.LocationId);
                lblTotal.Text = "Total products: " + onlineProducts.Count;
                TSLblStatus.Text = "Products have been loaded..";
                btnLoadProducts.Enabled = false;
                grpBxProductLoadOption.Enabled = true;

                MessageBox.Show("Products have been loaded!",
                   "Infomation", MessageBoxButtons.OK);


            }
        }

        private void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            if (rdioBtnSelectedMode == null)
            {
                MessageBox.Show("Select mode first, then confirm your selection.",
                    "Infomation", MessageBoxButtons.OK);
            }

            else
            {
                btnConfirmSelection.Text = "Selection Confirmed";
                grpBxModes.Enabled = false;
            }
        }

        private void txtBxBarcode_TextChanged(object sender, EventArgs e)
        {
            //if (txtBxBarcode.Text.Equals(string.Empty) == false)
            //{
            //    long barcode = long.Parse(txtBxBarcode.Text);

            //    updatedProducts = APIHelper.updateProduct(this, onlineProducts, barcode, 1);

            //    updatedProducts = updatedProducts.GroupBy(x => x.ProductID).Select(y => y.First()).ToList();
            //    dgvUpdatingProduct.DataSource = null;
            //    dgvUpdatingProduct.DataSource = updatedProducts;

            //    if (btnLoadCSV.Enabled == true)
            //    {
            //        btnLoadCSV.Enabled = false;
            //    }
            //}
        }

        private void btnLoadCSV_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "|*.csv";

            DialogResult result = fileDialog.ShowDialog(); // Show the dialog.

            if (result == DialogResult.OK) // Test result.
            {
                btnLoadCSV.Text = "\"" + fileDialog.SafeFileName + "\"";

                EposAPIHelper.CSV csv = new EposAPIHelper.CSV(this);
                productsCsv = csv.getProductsDGVFromCSV(fileDialog.FileName);
                txtBxBarcode.Enabled = false;

                updatedProducts = APIHelper.updateProduct(this, onlineProducts, productsCsv);

                updatedProducts = updatedProducts.GroupBy(x => x.ProductID).Select(y => y.First()).ToList();
                dgvUpdatingProduct.DataSource = null;
                dgvUpdatingProduct.DataSource = updatedProducts;

                if (txtBxBarcode.Enabled == true)
                {
                    txtBxBarcode.Enabled = false;
                }
            }
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            APIHelper.changeProducts(this, updatedProducts, productsCsv, rdioBtnSelectedMode.Text);
            MessageBox.Show("Products have bee updated!", "Information",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        DateTime lastKeyPress = DateTime.Now;
        private void txtBxBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            // its mean new barcodes first char has been wirtten, so remove the old barcode
            if (((TimeSpan)(DateTime.Now - lastKeyPress)).TotalMilliseconds > 10)
            {
                // Need to uncomment this
                //txtBxBarcode.Text = "";
            }

            if (e.KeyChar == Convert.ToChar(Keys.Return))
            {
                if (txtBxBarcode.Text.Equals(string.Empty) == false)
                {
                    long barcode = long.Parse(txtBxBarcode.Text);

                    updatedProducts.AddRange(APIHelper.updateProduct(this, onlineProducts, barcode, 1));

                    updatedProducts = updatedProducts.GroupBy(x => x.ProductID).Select(y => y.First()).ToList();
                    dgvUpdatingProduct.DataSource = null;
                    dgvUpdatingProduct.DataSource = updatedProducts;

                    if (btnLoadCSV.Enabled == true)
                    {
                        btnLoadCSV.Enabled = false;
                    }
                }
            }
            lastKeyPress = DateTime.Now;
        }

        private void btnExportSuccessProducts_Click(object sender, EventArgs e)
        {
            List<ProductDGV> productCSVList = (List<ProductDGV>)dgvUpdatingProduct.DataSource;

            EposAPIHelper.CSV csv = new EposAPIHelper.CSV(this);
            string path = "successfulProducts.csv";
            csv.exportProductDGVList(productCSVList, path);

            showMessageBox("Successful product list exported successfully", "List exported!");
        }

        private void showMessageBox(String message, string title)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnExportFailedProducts_Click(object sender, EventArgs e)
        {
            List<ProductDGV> productCSVList = (List<ProductDGV>)dgvFailedProduct.DataSource;

            EposAPIHelper.CSV csv = new EposAPIHelper.CSV(this);
            string path = "failedProducts.csv";
            csv.exportProductDGVList(productCSVList, path);

            showMessageBox("Failed product list exported successfully", "List exported!");
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            string body = "Selected Mode: " + rdioBtnSelectedMode.Text + "\r\n";
            body += "Location: " + GlobalClass.Location + "\r\n";
            body += "User: " + GlobalClass.CurrentUserName + " & Id: " + GlobalClass.CurrentUserId + "\r\n";
            body += "Time: " + DateTime.Now.ToString();
            string recipeint = GlobalClass.EmailRecipeint;
            string emailSender = GlobalClass.EmailSender;
            string password = GlobalClass.EmailPassword;
            string emailProvider = GlobalClass.EmailProvider;

            List<ProductDGV> productCSVList = (List<ProductDGV>)dgvUpdatingProduct.DataSource;

            EposAPIHelper.CSV csv = new EposAPIHelper.CSV(this);
            string path = "successfulProducts.csv";
            csv.exportProductDGVList(productCSVList, path);

            string fileName = path;

            APIHelper.mailTo(body, recipeint, emailSender, password, emailProvider, fileName);

            showMessageBox("\"Successful product list\" emailed to: " + GlobalClass.EmailRecipeint, "List emailed!");

        }
    }
}

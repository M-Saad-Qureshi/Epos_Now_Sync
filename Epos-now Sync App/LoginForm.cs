﻿using EposAPIHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Epos_now_Sync_App
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {                   
            Application.Exit();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            string user = TxtBxId.Text;
            string password= TxtBxPassword.Text;

            TSLblStatus.Text = "Authenticating user, please wait..";
            StaffResponse logginStaff = APIHelper.authenticateUser(this, user, password);

            if (logginStaff != null)
            {
                GlobalClass.IsUserLoggedIn = true;
                GlobalClass.CurrentUserName = user;
                GlobalClass.CurrentUserId = logginStaff.StaffID;

                GlobalClass.EmailRecipeint = ConfigurationManager.AppSettings["recipeint"].ToString();
                GlobalClass.EmailSender = ConfigurationManager.AppSettings["emailSender"].ToString();
                GlobalClass.EmailPassword = ConfigurationManager.AppSettings["password"].ToString();
                GlobalClass.EmailProvider = ConfigurationManager.AppSettings["emailProvider"].ToString();


                DialogResult = DialogResult.OK;
            }

            else
            {
                TSLblStatus.Text = "Login id or password incorrect";
                DialogResult = DialogResult.Cancel;
            }
        }
    }
}

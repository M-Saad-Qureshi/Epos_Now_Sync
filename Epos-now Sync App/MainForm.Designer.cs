﻿namespace Epos_now_Sync_App
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.rdioBtnModeAdd = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.grpBxModes = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnConfirmSelection = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnLoadProducts = new System.Windows.Forms.Button();
            this.dgvUpdatingProduct = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSync = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.TSLblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.grpBxProductLoadOption = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLoadCSV = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBxBarcode = new System.Windows.Forms.TextBox();
            this.dgvFailedProduct = new System.Windows.Forms.DataGridView();
            this.btnExportSuccessProducts = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnExportFailedProducts = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEmail = new System.Windows.Forms.Button();
            this.grpBxModes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdatingProduct)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.grpBxProductLoadOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFailedProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(95, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // rdioBtnModeAdd
            // 
            this.rdioBtnModeAdd.AutoSize = true;
            this.rdioBtnModeAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdioBtnModeAdd.Location = new System.Drawing.Point(12, 25);
            this.rdioBtnModeAdd.Name = "rdioBtnModeAdd";
            this.rdioBtnModeAdd.Size = new System.Drawing.Size(54, 21);
            this.rdioBtnModeAdd.TabIndex = 1;
            this.rdioBtnModeAdd.TabStop = true;
            this.rdioBtnModeAdd.Text = "Add";
            this.rdioBtnModeAdd.UseVisualStyleBackColor = true;
            this.rdioBtnModeAdd.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(12, 52);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(87, 21);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Subtract";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(12, 79);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(85, 21);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Replace";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // grpBxModes
            // 
            this.grpBxModes.Controls.Add(this.label2);
            this.grpBxModes.Controls.Add(this.btnConfirmSelection);
            this.grpBxModes.Controls.Add(this.rdioBtnModeAdd);
            this.grpBxModes.Controls.Add(this.radioButton2);
            this.grpBxModes.Controls.Add(this.radioButton1);
            this.grpBxModes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBxModes.Location = new System.Drawing.Point(0, 0);
            this.grpBxModes.Name = "grpBxModes";
            this.grpBxModes.Size = new System.Drawing.Size(489, 108);
            this.grpBxModes.TabIndex = 4;
            this.grpBxModes.TabStop = false;
            this.grpBxModes.Text = "Select mode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(417, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "==>";
            // 
            // btnConfirmSelection
            // 
            this.btnConfirmSelection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmSelection.Location = new System.Drawing.Point(164, 36);
            this.btnConfirmSelection.Name = "btnConfirmSelection";
            this.btnConfirmSelection.Size = new System.Drawing.Size(191, 43);
            this.btnConfirmSelection.TabIndex = 5;
            this.btnConfirmSelection.Text = "Confirm selection";
            this.btnConfirmSelection.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfirmSelection.UseVisualStyleBackColor = true;
            this.btnConfirmSelection.Click += new System.EventHandler(this.btnConfirmSelection_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(12, 2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grpBxModes);
            this.splitContainer1.Panel2.Controls.Add(this.btnLoadProducts);
            this.splitContainer1.Size = new System.Drawing.Size(1045, 108);
            this.splitContainer1.SplitterDistance = 262;
            this.splitContainer1.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(262, 108);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Current Location";
            // 
            // btnLoadProducts
            // 
            this.btnLoadProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadProducts.Location = new System.Drawing.Point(532, 37);
            this.btnLoadProducts.Name = "btnLoadProducts";
            this.btnLoadProducts.Size = new System.Drawing.Size(191, 43);
            this.btnLoadProducts.TabIndex = 6;
            this.btnLoadProducts.Text = "Load products from Epos-now";
            this.btnLoadProducts.UseVisualStyleBackColor = true;
            this.btnLoadProducts.Click += new System.EventHandler(this.btnLoadProducts_Click);
            // 
            // dgvUpdatingProduct
            // 
            this.dgvUpdatingProduct.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUpdatingProduct.BackgroundColor = System.Drawing.Color.White;
            this.dgvUpdatingProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpdatingProduct.Location = new System.Drawing.Point(6, 78);
            this.dgvUpdatingProduct.Name = "dgvUpdatingProduct";
            this.dgvUpdatingProduct.ReadOnly = true;
            this.dgvUpdatingProduct.RowHeadersVisible = false;
            this.dgvUpdatingProduct.Size = new System.Drawing.Size(1030, 165);
            this.dgvUpdatingProduct.TabIndex = 6;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnEmail);
            this.groupBox3.Controls.Add(this.btnExportFailedProducts);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.btnExportSuccessProducts);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.dgvFailedProduct);
            this.groupBox3.Controls.Add(this.btnSync);
            this.groupBox3.Controls.Add(this.lblTotal);
            this.groupBox3.Controls.Add(this.dgvUpdatingProduct);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 190);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1045, 493);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Manage Products";
            // 
            // btnSync
            // 
            this.btnSync.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSync.Location = new System.Drawing.Point(455, 13);
            this.btnSync.Name = "btnSync";
            this.btnSync.Size = new System.Drawing.Size(120, 32);
            this.btnSync.TabIndex = 10;
            this.btnSync.Text = "Sync";
            this.btnSync.UseVisualStyleBackColor = true;
            this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(6, 26);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(123, 17);
            this.lblTotal.TabIndex = 9;
            this.lblTotal.Text = "Total products: ";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSLblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 686);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1069, 22);
            this.statusStrip1.TabIndex = 13;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // TSLblStatus
            // 
            this.TSLblStatus.Name = "TSLblStatus";
            this.TSLblStatus.Size = new System.Drawing.Size(28, 17);
            this.TSLblStatus.Text = "       ";
            // 
            // grpBxProductLoadOption
            // 
            this.grpBxProductLoadOption.Controls.Add(this.label3);
            this.grpBxProductLoadOption.Controls.Add(this.btnLoadCSV);
            this.grpBxProductLoadOption.Controls.Add(this.label4);
            this.grpBxProductLoadOption.Controls.Add(this.txtBxBarcode);
            this.grpBxProductLoadOption.Enabled = false;
            this.grpBxProductLoadOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBxProductLoadOption.Location = new System.Drawing.Point(12, 113);
            this.grpBxProductLoadOption.Name = "grpBxProductLoadOption";
            this.grpBxProductLoadOption.Size = new System.Drawing.Size(1045, 76);
            this.grpBxProductLoadOption.TabIndex = 10;
            this.grpBxProductLoadOption.TabStop = false;
            this.grpBxProductLoadOption.Text = "Product load options";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(409, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Or";
            // 
            // btnLoadCSV
            // 
            this.btnLoadCSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadCSV.Location = new System.Drawing.Point(210, 26);
            this.btnLoadCSV.Name = "btnLoadCSV";
            this.btnLoadCSV.Size = new System.Drawing.Size(119, 32);
            this.btnLoadCSV.TabIndex = 6;
            this.btnLoadCSV.Text = "Load CSV";
            this.btnLoadCSV.UseVisualStyleBackColor = true;
            this.btnLoadCSV.Click += new System.EventHandler(this.btnLoadCSV_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(508, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(207, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Selected Product Barcode :";
            // 
            // txtBxBarcode
            // 
            this.txtBxBarcode.Location = new System.Drawing.Point(721, 30);
            this.txtBxBarcode.Name = "txtBxBarcode";
            this.txtBxBarcode.Size = new System.Drawing.Size(265, 24);
            this.txtBxBarcode.TabIndex = 8;
            this.txtBxBarcode.TextChanged += new System.EventHandler(this.txtBxBarcode_TextChanged);
            this.txtBxBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBxBarcode_KeyPress);
            // 
            // dgvFailedProduct
            // 
            this.dgvFailedProduct.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFailedProduct.BackgroundColor = System.Drawing.Color.White;
            this.dgvFailedProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFailedProduct.Location = new System.Drawing.Point(6, 277);
            this.dgvFailedProduct.Name = "dgvFailedProduct";
            this.dgvFailedProduct.ReadOnly = true;
            this.dgvFailedProduct.RowHeadersVisible = false;
            this.dgvFailedProduct.Size = new System.Drawing.Size(1030, 165);
            this.dgvFailedProduct.TabIndex = 11;
            // 
            // btnExportSuccessProducts
            // 
            this.btnExportSuccessProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportSuccessProducts.Location = new System.Drawing.Point(913, 46);
            this.btnExportSuccessProducts.Name = "btnExportSuccessProducts";
            this.btnExportSuccessProducts.Size = new System.Drawing.Size(123, 26);
            this.btnExportSuccessProducts.TabIndex = 13;
            this.btnExportSuccessProducts.Text = "Export to CSV";
            this.btnExportSuccessProducts.UseVisualStyleBackColor = true;
            this.btnExportSuccessProducts.Click += new System.EventHandler(this.btnExportSuccessProducts_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Green;
            this.label5.Location = new System.Drawing.Point(6, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(229, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Products successfully updated";
            // 
            // btnExportFailedProducts
            // 
            this.btnExportFailedProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportFailedProducts.Location = new System.Drawing.Point(913, 245);
            this.btnExportFailedProducts.Name = "btnExportFailedProducts";
            this.btnExportFailedProducts.Size = new System.Drawing.Size(123, 26);
            this.btnExportFailedProducts.TabIndex = 15;
            this.btnExportFailedProducts.Text = "Export to CSV";
            this.btnExportFailedProducts.UseVisualStyleBackColor = true;
            this.btnExportFailedProducts.Click += new System.EventHandler(this.btnExportFailedProducts_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label6.Location = new System.Drawing.Point(6, 254);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(191, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Products failed to update";
            // 
            // btnEmail
            // 
            this.btnEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmail.Location = new System.Drawing.Point(430, 451);
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(191, 32);
            this.btnEmail.TabIndex = 16;
            this.btnEmail.Text = "Email Updated CSV";
            this.btnEmail.UseVisualStyleBackColor = true;
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 708);
            this.Controls.Add(this.grpBxProductLoadOption);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpBxModes.ResumeLayout(false);
            this.grpBxModes.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdatingProduct)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.grpBxProductLoadOption.ResumeLayout(false);
            this.grpBxProductLoadOption.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFailedProduct)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdioBtnModeAdd;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.GroupBox grpBxModes;
        private System.Windows.Forms.Button btnConfirmSelection;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvUpdatingProduct;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnLoadProducts;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel TSLblStatus;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox grpBxProductLoadOption;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLoadCSV;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBxBarcode;
        private System.Windows.Forms.Button btnSync;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExportFailedProducts;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnExportSuccessProducts;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvFailedProduct;
        private System.Windows.Forms.Button btnEmail;
    }
}

